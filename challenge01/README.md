# Data manipulation challenge

This challenge is divided in parts but all are based on the same base scenario, 6 branch routers connected to each other. The goal of this excercise is to practice Python data structures.

## Part 1: Cutsheet and device facts

The goal is to parse a Cutsheet-style CSV file (`cutsheet.csv`) and create a data structure that holds the link data. The data structure at the end should:

- No ports being reused on a single router. For example it should not have duplicate links data.
- Have a valid interface name and value.

The number of interfaces available on each device is depicted in `facts.yml`, you should be able to use this information as a constraint for the interface name and value specified above.

**NOTE:** Is important to highlight that you should treat the `cutsheet.csv` file as a manually generated file prone to errors, while the `facts.yml` comes from a source of truth like a database (meaning that the data there is already validated).
