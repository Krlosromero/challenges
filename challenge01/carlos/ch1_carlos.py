#krlosromero 20200524
import csv
import yaml
from pprint import pprint


def csv_reader(file):
    """ usage
        csv_reader('cutsheet.csv')
        output
        [['r1', 'g0/0', 'r2', 'g0/0'],
        ['r1', 'g0/1', 'r3', 'g0/0'],
        ['r2', 'g0/0', 'r3', 'g0/0']],
    """

    with open(file, 'r') as csvfile:
        data =list()
        reader = csv.reader(csvfile, delimiter= ',')
        for line in reader:
            links = [value.strip('\ufeff') for value in line]
            data.append(links)
    return data


def yaml_reader(file):
    """usage:
    yaml_reader("facts.yml")
   {'device': 'r1',
     'model': 'virtual-router',
     'net_os': 'ios',
     'interfaces': ['Gi0/0',
      'Gi0/1',
      'Gi0/2',
      'Gi0/3',
      'Gi1/0',
      'Gi1/1',
      'Gi1/2',
      'Gi1/3']}
    """
    with open(file) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        # print(data)
    return data


def name_changer(net_os,interface):
    """
        usage: In [51]: name_changer("ios", "g7/7") 
        Out[51]: 'Eth7/7'
    """

    if net_os == "ios":
        interface_mod = interface.replace("g", "Gi")
    elif net_os == "eos":
        interface_mod = interface.replace("g", "Eth")
    else:
        print(f"net_os {net_os} not found for replacement")
    return interface_mod

def normalizer(row_cutsheet, net_facts):

    norm_cutsheet = list()
    
    for link in row_cutsheet:
        src = link[:2]
        dst = link[2:]
        src_good = True
        dst_good = True
        # print(f'Normalizing link entry:')
        # print(f' SRC { link[:2] } --> DST { link[2:] }') 
        
        try:        #checking router existance
            if (net_facts[src[0]] and net_facts[dst[0]]):
                #chnnging names
                net_os = net_facts[src[0]]["net_os"]
                src[1] = name_changer(net_os,src[1])
    
                net_os = net_facts[dst[0]]["net_os"]
                dst[1] = name_changer(net_os,dst[1])
                
                # norm_cutsheet.append(src + dst)

                                          #checking interfaces usage/existance
            if src[1] in net_facts[src[0]]["interfaces"]:
                net_facts[src[0]]["interfaces"].remove(src[1])
            else:
                print(f"interface {src[1]} on device {src[0]} is non existent or in use")
                src_good = False 

            if dst[1] in net_facts[dst[0]]["interfaces"]:
                net_facts[dst[0]]["interfaces"].remove(dst[1])
            else:
                print(f"interface {dst[1]} on device {dst[0]} is non existent or already in use")
                dst_good = False
    
        except KeyError as missed_router:
            print(f"Router {missed_router} not found in source of true plase check on that...!!!")
            src_good = False
            dst_good = False

        if (src_good == True and dst_good ==True):
            norm_cutsheet.append(src + dst)
    
    return(norm_cutsheet)


def main():
    # read row cutsheet
    row_cutsheet = csv_reader("../cutsheet.csv")

    print("\n Row Cutsheet: \n ")
    pprint(row_cutsheet)
    # reasd facts
    row_facts = yaml_reader("../facts.yml")

    # normalizing row_facts
    net_facts = {router["device"]: router for router in row_facts }

    print("\n Normalizing Cutsheet \n ")
    #normalizing the cutsheet 
    norm_cutsheet = normalizer(row_cutsheet, net_facts)   

    print("\n Clean Cutsheet: \n")
    pprint(norm_cutsheet)


if __name__ == "__main__":
    main()
