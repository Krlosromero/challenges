#!/usr/bin/env python
"""
Script that performs tasks depicted on challenge1
"""
import yaml
import csv
from pathlib import Path
from pprint import pprint

CUTSHEET = Path("../cutsheet.csv")
FACTS = Path("../facts.yml")


def read_yaml(path):
    with open(path, "r") as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data


def read_csv(path):
    data = []
    with open(path, "r") as f:
        data_reader = csv.reader(f,)
        for row in data_reader:
            data.append([item.strip("\ufeff") for item in row])
    return data


def interface_converter(name, net_os):
    if net_os == "ios":
        return name.replace("g", "Gi")
    elif net_os == "eos":
        return name.replace("g", "Eth")


def main():
    # Load data from cutsheet and facts
    facts = read_yaml(FACTS)
    cutsheet = read_csv(CUTSHEET)

    # Create Routers facts data structure
    routers_facts = {dev["device"]: dev for dev in facts}

    # Create Links data structure
    links = []
    # Flags that are used as contraints to validate the links data
    reused = False
    valid_entry = True
    for link_data in cutsheet:
        endpoint_a = link_data[:2]
        endpoint_b = link_data[-2:]

        # Verify and format device and interface on an endpoint are valid
        for endpoint in [endpoint_a, endpoint_b]:
            try:
                router = endpoint[0]
                # Get routers avaialble interfaces
                available_interfaces = routers_facts[router]["interfaces"]

                # Format interface name
                interface = interface_converter(
                    endpoint[-1], net_os=routers_facts[router]["net_os"]
                )

                if interface not in available_interfaces:
                    print(
                        f"[ERROR] Interface {interface} not valid for {router} - see "
                        f"available interfaces in facts.yml"
                    )
                    valid_entry = False
                # Save formatted interface name
                endpoint[-1] = interface
            except KeyError as err:
                print(f"[ERROR] Router {str(err)} not in facts.yml")
                valid_entry = False

        # Verify endpoints are not being reused
        for stored_link in links:
            if endpoint_a == stored_link[:2] or endpoint_a == stored_link[-2:]:
                print(
                    f"[ERROR] Link {endpoint_a} is already used. Cannot add {link_data}"
                )
                reused = True
            elif endpoint_b == stored_link[:2] or endpoint_b == stored_link[-2:]:
                print(
                    f"[ERROR] Link {endpoint_b} is already used. Cannot add {link_data}"
                )
                reused = True

        # Save Link info if endpoint is not already there
        if not reused and valid_entry:
            links.append(endpoint_a + endpoint_b)

        # Re-initialize flags
        reused = False
        valid_entry = True
    print("\nResulting Links:")
    pprint(links)


if __name__ == "__main__":
    main()
